# Address Book

GIG JavaScript Challenge - Client Side Address Book

Build a Single Page Application (SPA) address book using JavaScript, HTML and CSS.


## Run the demo

Live demo is available at this link [http://demo.gcirone.com/address-book](http://demo.gcirone.com/address-book)


In alternative is possible to install a static server like [lite-server](https://www.npmjs.com/package/lite-server) to test the demo.

    $ npm install lite-server -g && lite-server --baseDir=public

It will open a browser in automatic at this url [http://localhost:3000](http://localhost:3000)


### Development

To run the application in development environment

    $  npm run serve
    
It will open a browser in automatic at this url [http://localhost:3000](http://localhost:3300)



### Build

To build the application for production

    $  npm run build
    
The process generate a dist folder that contain the production version of the application.


# Test

To run the unit test

    $  npm run test
    
The process generate a coverage folder that contain istanbul reports for the test.

***Current code coverage***
```text
=============================== Coverage summary ===============================
Statements   : 97.77% ( 219/224 )
Branches     : 79.41% ( 27/34 )
Functions    : 98.51% ( 66/67 )
Lines        : 97.49% ( 194/199 )
================================================================================
Executed 22 of 22 SUCCESS (1.448 secs / 1.414 secs)
```


### E2E

To un the e2e test

    $  npm run e2e

### Technologies used

The project use some cutting edge technology to provide the best experience to the users involved in.

*  Node JS - used to manage the build and test
*  Yarn or Npm - used to manage the module dependencies
*  Typescript - used by Angular to create scalable javascript application
*  MobX - used by Angular to manage the app state
*  Sass - used to generate stylesheets for the application
*  TS Lint / Style Lint - used for improve the source code quality  
*  Karma / Jasmine - used to test the source code of application
*  Protractor - used for e2e test behaviour

### Prerequisites

The project have dependencies that require [Node](https://nodejs.org/en/) 6.9.1 or higher, together with NPM 3 or higher. [Yarn](https://yarnpkg.com/en/) is optional but highly recommended, if you prefer to work with npm directly you may disable it with CLI options.

* Node **>= 6.9.1**
* Npm or Yarn

*P.S.: For Windows user is recommended the [Git Bash](https://www.youtube.com/watch?v=rWboGsc6CqI) for using the NG CLI.*


### Note

Address Book is inspired by the native MacOS Contact app. 
