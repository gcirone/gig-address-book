// Karma configuration file

const stringArgv = process.argv.slice(2).join();
const isSingleRun = stringArgv.includes('--single-run');
const currentReporter =  isSingleRun ? 'spec': 'progress' ;

module.exports = function (config) {
  config.set({
    basePath: process.cwd(),
    frameworks: ['jasmine', '@angular/cli'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('karma-spec-reporter'),
      require('@angular/cli/plugins/karma')
    ],
    client:{
      clearContext: false
    },
    coverageIstanbulReporter: {
      reports: [ 'html', 'lcovonly', 'text-summary'],
      fixWebpackSourcePaths: true,
      dir: 'coverage',
    },
    angularCli: {
      environment: 'dev',
      codeCoverage: true,
      progress: false
    },
    reporters: [currentReporter, 'kjhtml'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    browserDisconnectTolerance: 3,
    browserNoActivityTimeout: 50000,
    autoWatch: true,
    browsers: ['HeadlessChrome'],
    customLaunchers: {
      HeadlessChrome: {
        base: 'ChromeHeadless',
        flags: [
          '--disable-gpu', '--disable-translate', '--disable-extensions', '--disable-web-security',
          '--disable-default-apps', '--no-sandbox', '--disable-setuid-sandbox', '--blink-settings=imagesEnabled=false',
        ]
      }
    },
    singleRun: false,
    concurrency: Infinity
  });
};
