import { TestBed } from '@angular/core/testing';
import { CountryListService } from '../../../../src/app/core/data/country-list.service';

describe('CountryListService', () => {
  let countryListService: CountryListService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CountryListService]
    });

    countryListService = TestBed.get(CountryListService);
  });

  it('#allCountries should return the full list of countries', () => {
    expect(countryListService.allCountries.length).toBe(249);
  });

  it('#getName should return the name of the country by code', () => {
    expect(countryListService.getName('IT')).toBe('Italy');
  });
});
