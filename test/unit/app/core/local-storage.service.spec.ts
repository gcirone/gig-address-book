import { TestBed } from '@angular/core/testing';
import { LocalStorageService } from '../../../../src/app/core/data/local-storage.service';

describe('LocalStorageService', () => {
  let localStorageService: LocalStorageService;

  const dataKey = 'data-key';

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LocalStorageService]
    });

    spyOn(localStorage, 'getItem').and.callThrough();
    spyOn(localStorage, 'setItem').and.callThrough();
    spyOn(localStorage, 'removeItem').and.callThrough();

    localStorageService = TestBed.get(LocalStorageService);
  });

  it('#set should set the local storage data', () => {
    localStorageService.set(dataKey, 'value');

    expect(localStorage.setItem).toHaveBeenCalledWith(dataKey, 'value');
  });

  it('#get should get the local storage data', () => {
    expect(localStorageService.get(dataKey)).toBe('value');
  });

  it('#remove should remove the local storage data', () => {
    localStorageService.remove(dataKey);

    expect(localStorageService.get(dataKey)).toBeNull();
  });
});
