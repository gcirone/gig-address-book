import { TestBed } from '@angular/core/testing';
import { ContactListService } from '../../../../src/app/core/contact-list.service';
import { LocalStorageService } from '../../../../src/app/core/data/local-storage.service';
import { CONTACT_FIXTURES } from '../../../../src/app/core/contact-list.fixture';

describe('ContactListService', () => {
  let contactListService: ContactListService;
  let localStorageService: LocalStorageService;

  const storageKey = 'gig-contacts';
  const storageDataMock = [
    { firstName: 'bfn', lastName: 'ln', email: 'g@g.g', country: 'ND', selected: true },
    { firstName: 'afn2', lastName: 'ln2', email: 'g2@g.g', country: 'ND' }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ContactListService,
        { provide: LocalStorageService, useValue: { get() {}, set() {} } }
      ]
    });
  });

  it('should set the default contacts if not present', () => {
    givenContactListService(null);

    const expectedData = JSON.stringify(CONTACT_FIXTURES);
    expect(localStorageService.set).toHaveBeenCalledWith(storageKey, expectedData);
  });

  it('#contacts should return the correct list of contacts', () => {
    givenContactListService();

    expect(localStorageService.get).toHaveBeenCalledWith(storageKey);
    thenContactsFullNameAre(['bfn ln', 'afn2 ln2']);
  });

  it('#order should order alphabetically the contacts', () => {
    givenContactListService();

    contactListService.order();
    thenContactsFullNameAre(['afn2 ln2', 'bfn ln']);
  });

  describe('#search', () => {
    beforeEach(() => givenContactListService());

    it('should filter the results by full name that match the term', () => {
      contactListService.search('af');

      thenContactsFullNameAre(['afn2 ln2']);
    });

    it('should restore the results when the search term is empty', () => {
      const searchFlow = ['tg', 'pr', '']
        .forEach(term => contactListService.search(term));

      thenContactsFullNameAre(['bfn ln', 'afn2 ln2']);
    });
  });

  function thenContactsFullNameAre(fullNames: string[]) {
    expect(contactListService.contacts.map(contact => contact.fullName))
      .toEqual(fullNames);
  }

  function givenContactListService(storageData = storageDataMock) {
    localStorageService = TestBed.get(LocalStorageService);
    spyOn(localStorageService, 'get').and.returnValue(JSON.stringify(storageData));
    spyOn(localStorageService, 'set');

    contactListService = TestBed.get(ContactListService);
  }
});
