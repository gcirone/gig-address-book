import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { AddressBookComponent } from '../../../../src/app/address-book/address-book.component';

describe('AddressBookComponent', () => {
  let fixture: ComponentFixture<AddressBookComponent>;
  let component: AddressBookComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AddressBookComponent],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(AddressBookComponent);
    component = fixture.componentInstance;
  });

  it('should create the component', () => {
    expect(component instanceof AddressBookComponent).toBeTruthy();
  });
});
