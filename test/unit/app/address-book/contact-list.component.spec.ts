import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { By } from '@angular/platform-browser' ;
import { ContactListComponent } from '../../../../src/app/address-book/contact-list/contact-list.component';
import { ContactListService } from '../../../../src/app/core/contact-list.service';
import { CONTACT_FIXTURES } from '../../../../src/app/core/contact-list.fixture';
import { Contact } from '../../../../src/app/address-book/model/contact.model';
import { MobxAngularModule } from 'mobx-angular';
import { observable } from 'mobx';

describe('ContactListComponent', () => {
  let fixture: ComponentFixture<ContactListComponent>;
  let component: ContactListComponent;

  let contactListService: ContactListService;
  const contactListMock: any = observable(CONTACT_FIXTURES);
  const contactMock = { firstName: 'Mock', lastName: 'Mock', email: 'm@m.mo', country: 'US' };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MobxAngularModule],
      declarations: [ContactListComponent],
      providers: [
        { provide: ContactListService, useValue: { contacts: contactListMock }}
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(ContactListComponent);
    component = fixture.componentInstance;

    contactListService = TestBed.get(ContactListService);

    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component instanceof ContactListComponent).toBeTruthy();
  });

  it('should render the contact list correctly', () => {
    const contacts = getListGroupDetails();

    expect(contacts.length).toBe(CONTACT_FIXTURES.length);
  });

  it('should render the contact with full name', () => {
    contactListMock.replace([...CONTACT_FIXTURES, Contact.empty()]);

    const fullNames = getListGroupDetails().map(contact => contact.listName);

    expect(fullNames).toEqual(['Gianluca Cirone', 'No Name']);
  });

  it('should select the clicked contact', () => {
    contactListMock.replace([contactMock, ...CONTACT_FIXTURES, Contact.empty()]);

    const contacts = getListGroupDetails();
    contacts[0].trigger('click', null);

    expect(getListGroupDetails()[0].active).toBeTruthy();
  });

  function getListGroupDetails() {
    const details = (value) => {
      return {
        active: value.nativeElement.classList.contains('active'),
        listName: value.query(By.css('.list-name')).nativeElement.textContent.trim(),
        trigger: (eventName: string, eventObj: any) => value.triggerEventHandler(eventName, eventObj)
      };
    };

    return getListGroupItems().map(details);
  }

  function getListGroupItems() {
    return fixture.debugElement.queryAll(By.css('.list-group-item'));
  }
});
