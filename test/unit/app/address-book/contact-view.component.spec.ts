import { By } from '@angular/platform-browser' ;
import { ReactiveFormsModule } from '@angular/forms';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { ContactViewComponent } from '../../../../src/app/address-book/contact-view/contact-view.component';
import { ContactFormComponent } from '../../../../src/app/address-book/contact-view/form/contact-form.component';
import { ContactState } from '../../../../src/app/address-book/model/contact-state.model';
import { ContactListService } from '../../../../src/app/core/contact-list.service';
import { CountryListService } from '../../../../src/app/core/data/country-list.service';
import { CONTACT_FIXTURES } from '../../../../src/app/core/contact-list.fixture';
import { Contact } from '../../../../src/app/address-book/model/contact.model';
import { MobxAngularModule } from 'mobx-angular';
import { observable } from 'mobx';

describe('ContactViewComponent FormIntegration', () => {
  let fixture: ComponentFixture<ContactViewComponent>;
  let component: ContactViewComponent;

  let contactListService: ContactListService;
  const contactListMock: any = observable(CONTACT_FIXTURES.map(contact => new Contact(contact)));
  const contactMock = { firstName: 'Mock', lastName: 'Mock', email: 'm@m.mo', country: 'US' };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MobxAngularModule, ReactiveFormsModule],
      declarations: [ContactFormComponent, ContactViewComponent],
      providers: [
        CountryListService,
        { provide: ContactListService, useValue: { contacts: contactListMock, order() {} }}
      ]
    });

    fixture = TestBed.createComponent(ContactViewComponent);
    component = fixture.componentInstance;

    contactListService = TestBed.get(ContactListService);

    fixture.detectChanges();
  });

  it('should create the component and the form', () => {
    expect(component instanceof ContactViewComponent).toBeTruthy();
  });

  it('should render the header info correctly', () => {
    const headerInfo = getHeaderInfo();

    expect(headerInfo.nickname).toEqual('GC');
    expect(headerInfo.name).toEqual('Gianluca Cirone');
    expect(headerInfo.country).toEqual('Italy (IT)');
  });

  it('should add new contact correctly', () => {
    triggerButtonClick('add');

    expect(contactListService.contacts.length).toBe(2);
    expect(contactListService.contacts[1].selected).toBeTruthy();
    expect(contactListService.contacts[1].state).toBe(ContactState.Editing);
  });

  it('should edit the selected contact', () => {
    component.selectedContact.state = ContactState.Saved;
    triggerButtonClick('edit');

    expect(component.selectedContact.state).toBe(ContactState.Editing);
    expect(component.formComponent.contactForm.enabled).toBeTruthy();
  });

  it('should save the edited contact if the form is valid', () => {
    component.formComponent.contactForm.patchValue(contactMock);
    triggerButtonClick('save');

    expect(component.selectedContact.abbrName).toEqual('MM');
    expect(component.selectedContact.state).toBe(ContactState.Saved);
    expect(component.formComponent.contactForm.disabled).toBeTruthy();
  });

  it('should remove the selected contact', () => {
    spyOn(window, 'confirm').and.returnValue(true);
    triggerButtonClick('edit');
    triggerButtonClick('remove');

    const expectedMessage = 'Are you sure to delete this contact?';
    expect(window.confirm).toHaveBeenCalledWith(expectedMessage);
    expect(contactListService.contacts.length).toBe(1);
  });

  function triggerButtonClick(name: string) {
    const btn = getButtonElement(name);

    btn.triggerEventHandler('click',  null);
  }

  function getHeaderInfo() {
    return {
      nickname: fixture.nativeElement.querySelector('.contact-nickname').textContent.trim(),
      name: fixture.nativeElement.querySelector('.contact-name').textContent.trim(),
      country: fixture.nativeElement.querySelector('.contact-country').textContent.trim(),
    };
  }

  function getButtonElement(name: string) {
    return fixture.debugElement.query(By.css(`button.btn-${name}`));
  }
});
