import { AppPage } from '../app.po';

describe('AddressBook App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display the default contact selected', () => {
    page.navigateTo();
    expect(page.getContactNameText()).toEqual('Gianluca Cirone');
  });
});
