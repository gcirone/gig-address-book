import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getContactNameText() {
    return element(by.css('.contact-name')).getText();
  }
}
