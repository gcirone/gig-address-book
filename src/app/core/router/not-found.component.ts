import { Component } from '@angular/core';

@Component({
  selector: 'gig-not-found',
  templateUrl: 'not-found.component.html',
})
export class NotFoundComponent { }
