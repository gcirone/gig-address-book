import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {

  constructor() {
    if (!this.isSupported()) {
      console.error('Local storage is not available in your browser!');
    }
  }

  get(key: string) {
    return localStorage.getItem(key);
  }

  set(key: string, value: string) {
    localStorage.setItem(key, value);
  }

  remove(key: string) {
    localStorage.removeItem(key);
  }

  isSupported(): boolean {
    const item = 'test';

    try {
      localStorage.setItem(item, item);
      localStorage.removeItem(item);
    } catch (e) {
      return false;
    }

    return true;
  }
}
