import { Injectable } from '@angular/core';
import * as CountryList from 'country-list';

export interface Country {
  code: string;
  name: string;
}

@Injectable()
export class CountryListService {
  private countryList: any;

  constructor() {
    this.countryList = new CountryList();
  }

  get allCountries(): Country[] {
    return this.countryList.getData();
  }

  getName(code: string): string {
    return this.countryList.getName(code);
  }
}
