import { Injectable } from '@angular/core';
import { Contact } from '../address-book/model/contact.model';
import { LocalStorageService } from './data/local-storage.service';
import { CONTACT_FIXTURES } from './contact-list.fixture';
import { action, observable } from 'mobx-angular';
import { IObservableArray, reaction, toJS } from 'mobx';

@Injectable()
export class ContactListService {
  @observable contacts: Contact[];
  private readonly contactListKey = 'gig-contacts';
  private savedContacts: Contact[];

  constructor(private localStorageService: LocalStorageService) {
    this.loadFixtures();
    this.contacts = this.getData();

    const contactsReaction = () => [
      this.contacts.map(contact => contact.selected),
      this.contacts.map(contact => contact.state)
    ];
    reaction(contactsReaction, () => {
      if (!this.savedContacts) {
        this.persistData();
      }
    });
  }

  @action order() {
    (this.contacts as IObservableArray<Contact>)
      .replace(this.contacts.sort(this.sortAlphabetically));
  }

  @action search(term: string) {
    if (term) {
      this.cacheSavedContacts();

      const matchResults = this.savedContacts
        .filter(contact =>
          contact.fullName.toUpperCase().includes(term.toUpperCase()));

      (this.contacts as IObservableArray<Contact>)
        .replace(this.selectFirstContact(matchResults));

    } else {
      (this.contacts as IObservableArray<Contact>)
        .replace(this.selectFirstContact(this.savedContacts));

      this.savedContacts = undefined;
    }
  }

  private getData(): Contact[] {
    const contacts = this.getLocalStorageData() || [];
    return contacts.map(contact => new Contact(contact));
  }

  private persistData() {
    const contactsMapping = toJS(this.contacts)
      .filter(contact => contact.fullName)
      .map((contact) => {
        return {
          firstName: contact.firstName,
          lastName: contact.lastName,
          email: contact.email,
          country: contact.country,
          note: contact.note,
          selected: contact.selected
        };
      });

    this.setLocalStorageData(contactsMapping);
  }

  private cacheSavedContacts() {
    if (!this.savedContacts) {
      this.savedContacts = [...this.contacts];
    }
  }

  private selectFirstContact(contacts: Contact[]) {
    contacts.forEach((contact, i) =>
      contact.selected = i === 0);

    return contacts;
  }

  private loadFixtures() {
    if (!this.getLocalStorageData()) {
      this.setLocalStorageData(CONTACT_FIXTURES);
    }
  }

  private getLocalStorageData() {
    return JSON.parse(this.localStorageService.get(this.contactListKey));
  }

  private setLocalStorageData(data: any) {
    this.localStorageService.set(this.contactListKey, JSON.stringify(data));
  }

  private sortAlphabetically(a: Contact, b: Contact) {
    const nameA = a.fullName.toUpperCase();
    const nameB = b.fullName.toUpperCase();

    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
    return 0;
  }
}
