export const CONTACT_FIXTURES: any = [
  {
    firstName: 'Gianluca',
    lastName: 'Cirone',
    email: 'gianluca.cirone@gmail.com',
    country: 'IT',
    note: 'just simple note',
    selected: true
  }
];
