import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef,
  OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ContactListService } from '../../../core/contact-list.service';
import { CountryListService } from '../../../core/data/country-list.service';
import { Contact } from '../../model/contact.model';
import { IReactionDisposer, reaction } from 'mobx';
import { computed } from 'mobx-angular';

@Component({
  selector: 'gig-contact-form',
  templateUrl: 'contact-form.component.html',
  styleUrls: ['contact-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContactFormComponent implements OnInit, OnDestroy {
  @ViewChild('firstName') firstNameInput: ElementRef;
  contactForm: FormGroup;

  private reactionDisposer: IReactionDisposer;

  constructor(public countryListService: CountryListService,
              private formBuilder: FormBuilder,
              private contactListService: ContactListService,
              private cd: ChangeDetectorRef) {}

  ngOnInit() {
    this.buildForm();

    this.reactionDisposer = reaction(
      () => this.selectedContact,
      () => this.rebuildForm());
  }

  ngOnDestroy() {
    if (this.reactionDisposer) {
      this.reactionDisposer();
    }
  }

  @computed get selectedContact(): Contact {
    return this.contactListService.contacts.find(contact => contact.selected);
  }

  private buildForm() {
    this.contactForm = this.formBuilder.group({
      firstName: [this.selectedContact.firstName, Validators.required],
      lastName: [this.selectedContact.lastName, Validators.required],
      email: [this.selectedContact.email, Validators.email],
      country: [this.selectedContact.country, Validators.required],
      note: this.selectedContact.note
    });

    this.finalBuildForm();
  }

  private rebuildForm() {
    this.contactForm.reset(this.selectedContact);
    this.finalBuildForm();
  }

  private finalBuildForm() {
    if (this.selectedContact && this.selectedContact.isSaved) {
      this.contactForm.disable();
    } else {
      this.contactForm.enable();
    }

    this.cd.markForCheck();

    if (this.firstNameInput) {
      this.firstNameInput.nativeElement.focus();
    }
  }
}
