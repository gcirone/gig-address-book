import { ChangeDetectionStrategy, Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { ContactListService } from '../../core/contact-list.service';
import { ContactFormComponent } from './form/contact-form.component';
import { CountryListService } from '../../core/data/country-list.service';
import { Contact } from '../model/contact.model';
import { ContactState } from '../model/contact-state.model';
import { action, computed } from 'mobx-angular';
import { IObservableArray } from 'mobx';

@Component({
  selector: 'gig-contact-view',
  templateUrl: 'contact-view.component.html',
  styleUrls: ['contact-view.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContactViewComponent {
  @ViewChild(ContactFormComponent) formComponent: ContactFormComponent;

  constructor(public countryListService: CountryListService,
              private contactListService: ContactListService) {}

  @computed get selectedContact(): Contact {
    return this.contactListService.contacts.find(contact => contact.selected);
  }

  @action addContact() {
    if (this.selectedContact) {
      this.selectedContact.selected = false;
    }

    const newContact = Contact.empty();
    newContact.selected = true;
    newContact.state = ContactState.Editing;
    this.contactListService.contacts.push(newContact);
  }

  @action editContact() {
    this.selectedContact.state = ContactState.Editing;
    this.formComponent.contactForm.enable();
    this.formComponent.firstNameInput.nativeElement.focus();
  }

  @action saveContact() {
    if (this.formComponent.contactForm.valid) {
      const contactToSave = this.formComponent.contactForm.value;
      this.selectedContact.merge(contactToSave);
      this.formComponent.contactForm.disable();
      this.contactListService.order();
      this.selectedContact.state = ContactState.Saved;
    }
  }

  @action removeContact() {
    if (confirm('Are you sure to delete this contact?')) {
      (this.contactListService.contacts as IObservableArray<Contact>)
        .remove(this.selectedContact);
    }
  }

  mailContact() {
    location.href = `mailto:${this.selectedContact.email}?subject=Hi!`;
  }
}
