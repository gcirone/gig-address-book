import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';
import { ContactListService } from '../../core/contact-list.service';
import { Subject } from 'rxjs/Subject';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'gig-contact-search',
  templateUrl: 'contact-search.component.html',
  styleUrls: ['contact-search.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContactSearchComponent {
  term = new Subject<string>();

  constructor(contactListService: ContactListService) {
    this.term.asObservable()
      .pipe(debounceTime(150), distinctUntilChanged())
      .subscribe(term => contactListService.search(term));
  }
}
