import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';
import { ContactListService } from '../../core/contact-list.service';
import { Contact } from '../model/contact.model';
import { action, computed } from 'mobx-angular';

@Component({
  selector: 'gig-contact-list',
  templateUrl: 'contact-list.component.html',
  styleUrls: ['contact-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContactListComponent {
  constructor(private contactListService: ContactListService) {}

  @computed get contacts(): Contact[] {
    return this.contactListService.contacts;
  }

  @action select(contactToSelect: Contact) {
    this.contacts.forEach(contact =>
      contact.selected = contact === contactToSelect);
  }
}
