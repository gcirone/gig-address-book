import { observable, computed } from 'mobx-angular';
import { ContactState } from './contact-state.model';

export interface ContactModel {
  firstName: string;
  lastName: string;
  readonly fullName?: string;
  readonly abbrName?: string;
  email: string;
  country: string;
  note?: string;
  selected?: boolean;
  state?: ContactState;
}

export class Contact implements ContactModel {
  firstName: string;
  lastName: string;
  email: string;
  country: string;
  note?: string;
  @observable selected?: boolean;
  @observable state?: ContactState;

  static empty(): Contact {
    return new Contact({ firstName: '', lastName: '', email: '', country: '' });
  }

  constructor(contact?: ContactModel) {
    this.merge(contact);
  }

  get fullName() {
    return (this.firstName + ' ' + this.lastName).trim();
  }

  get abbrName() {
    return this.firstName.substr(0, 1) + this.lastName.substr(0, 1);
  }

  @computed get isEditing() {
    return this.state === ContactState.Editing;
  }

  @computed get isSaved() {
    return this.state === ContactState.Saved || this.state === undefined;
  }

  merge(contact: ContactModel) {
    Object.assign<Contact, ContactModel>(this, contact);
  }
}
