export enum ContactState {
  Editing = 'editing',
  Saved = 'saved'
}
