import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddressBookRouterModule } from './address-book-router.module';
import { AddressBookComponent } from './address-book.component';
import { ContactSearchComponent } from './contact-search/contact-search.component';
import { ContactListComponent } from './contact-list/contact-list.component';
import { ContactViewComponent } from './contact-view/contact-view.component';
import { ContactListService } from '../core/contact-list.service';
import { MobxAngularModule } from 'mobx-angular';
import { ContactFormComponent } from './contact-view/form/contact-form.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AddressBookRouterModule,
    MobxAngularModule
  ],
  declarations: [
    AddressBookComponent,
    ContactSearchComponent,
    ContactListComponent,
    ContactViewComponent,
    ContactFormComponent
  ],
  providers: [
    ContactListService
  ]
})
export class AddressBookModule {}
