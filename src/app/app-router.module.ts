import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from './core/router/not-found.component';

export const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'address-book' },
  { path: 'address-book', loadChildren: './address-book/address-book.module#AddressBookModule' },

  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  declarations: [NotFoundComponent],
  exports: [RouterModule],
})
export class AppRouterModule { }
