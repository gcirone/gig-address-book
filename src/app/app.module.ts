import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRouterModule } from './app-router.module';
import { LocalStorageService } from './core/data/local-storage.service';
import { CountryListService } from './core/data/country-list.service';
import { AppComponent } from './app.component';

@NgModule({
  imports: [
    BrowserModule,
    AppRouterModule
  ],
  declarations: [
    AppComponent
  ],
  providers: [
    LocalStorageService,
    CountryListService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
